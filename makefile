ARCH?=x86_64

ROOT_DIR=$(shell pwd)
BUILD_ROOT=$(ROOT_DIR)/build

BUILD_DIR=$(BUILD_ROOT)/$(ARCH)
SRC_DIR=$(ROOT_DIR)/src/$(ARCH)

BOOTLOADER_DIR=$(BUILD_DIR)/bootloader
BOOTLOADER_SRC=$(SRC_DIR)/bootloader

BOOT=$(BOOTLOADER_DIR)/boot-$(ARCH).bin
ISO=$(BUILD_DIR)/os-$(ARCH).iso

GRUB_CFG=$(BOOTLOADER_SRC)/grub.cfg
LINKER_TEMPLATE=$(SRC_DIR)/linker/linker.ld

ASSEMBLY_SRC=$(wildcard $(BOOTLOADER_SRC)/*.asm)
ASSEMBLY_OBJ=$(patsubst $(BOOTLOADER_SRC)/%.asm, \
	$(BOOTLOADER_DIR)/%.o, $(ASSEMBLY_SRC))

OS=$(BUILD_DIR)/$(ARCH)-unknown-linux-gnu/release/libos.a
OS_SRC=$(SRC_DIR)/os
CARGO_FLAGS=--target $(ARCH)-unknown-linux-gnu --release

all: $(BOOT)

clean:
	@rm -rf $(BUILD_ROOT)

run: $(ISO)
	@qemu-system-x86_64 -cdrom $(ISO)

iso: $(ISO)

$(ISO): $(BOOT) $(GRUB_CFG)
	@mkdir -p $(BUILD_DIR)/isofiles/boot/grub
	@cp $(BOOT) $(BUILD_DIR)/isofiles/boot/boot.bin
	@cp $(GRUB_CFG) $(BUILD_DIR)/isofiles/boot/grub
	@grub-mkrescue -o $(ISO) $(BUILD_DIR)/isofiles 2> /dev/null
	@rm -rf $(BUILD_DIR)/isofiles

$(BOOT): os $(OS) $(ASSEMBLY_OBJ) $(LINKER_TEMPLATE)
	@ld -n --gc-sections -T $(LINKER_TEMPLATE) -o $(BOOT) \
		$(ASSEMBLY_OBJ) $(OS)

os:
	@cd $(OS_SRC) && cargo build $(CARGO_FLAGS) && cd $(ROOT_DIR)

# compile assembly files
$(BOOTLOADER_DIR)/%.o: $(BOOTLOADER_SRC)/%.asm
	@mkdir -p $(BOOTLOADER_DIR)
	@nasm -felf64 $< -o $@

.PHONY: all clean run iso