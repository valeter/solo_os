#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
pub fn read_byte_from_port(port: usize) -> u8 {
    let result: u8;
    asm!("in %dx, %al" : "={al}"(result) : "{dx}"(port));
    result
}

#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
pub fn write_byte_to_port(port: usize, data: u8) {
    asm!("out %al, %dx" : "{al}"(data) : "{dx}"(port));
}

#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
pub fn read_word_from_port(port: usize) -> usize {
    let result: u8;
    asm!("in %dx, %ax" : "={al}"(result) : "{dx}"(port));
    result
}

#[cfg(any(target_arch = "x86", target_arch = "x86_64"))]
pub fn write_word_to_port(port: usize, data: usize) {
    asm!("out %ax, %dx" : "{al}"(data) : "{dx}"(port));
}