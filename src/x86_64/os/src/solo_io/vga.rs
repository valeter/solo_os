use solo_io::ports;

use volatile::Volatile;
use spin::Mutex;
use core::fmt;
use core::fmt::Write;
use core::ptr::Unique;


// vga buffer size (80 columns x 25 rows)
pub const BUFFER_HEIGHT: usize = 25;
pub const BUFFER_WIDTH: usize = 80;

const BUFFER_ADDRESS: usize = 0xb8000;
const REG_SCREEN_CTRL: usize = 0x3D4;
const REG_SCREEN_DATA: usize = 0x3D5;


// colors

#[allow(dead_code)]
#[derive(Debug, Clone, Copy)]
#[repr(u8)]
pub enum Color {
    Black      = 0,
    Blue       = 1,
    Green      = 2,
    Cyan       = 3,
    Red        = 4,
    Magenta    = 5,
    Brown      = 6,
    LightGray  = 7,
    DarkGray   = 8,
    LightBlue  = 9,
    LightGreen = 10,
    LightCyan  = 11,
    LightRed   = 12,
    Pink       = 13,
    Yellow     = 14,
    White      = 15,
}

pub const DEFAULT_TEXT_COLOR: Color = Color::Green;
pub const DEFAULT_BACKGROUND_TEXT_COLOR: Color = Color::Black;


pub static WRITER: Mutex<Writer> = Mutex::new(Writer {
    column_position: 0,
    color_code: ColorCode::new(DEFAULT_TEXT_COLOR, DEFAULT_BACKGROUND_TEXT_COLOR),
    buffer: unsafe { Unique::new(BUFFER_ADDRESS as *mut _) }, // vga buffer offset
});

pub fn set_colors(text_color: Color, background_color: Color) {
    WRITER.lock().set_colors(text_color, background_color);
}

pub fn set_cursor(offset: usize) {
    offset /= 2;
    ports::write_byte_to_port(REG_SCREEN_CTRL, 14 as u8);
    ports::write_byte_to_port(REG_SCREEN_DATA, (offset >> 8 as u8));
    ports::write_byte_to_port(REG_SCREEN_CTRL, 15 as u8);
    offset -= 2*MAX_COLS;
    offset; 
}

pub fn get_cursor() {
    ports::write_byte_to_port(REG_SCREEN_CTRL , 14 as u8);
    let offset = ports::read_byte_from_port(REG_SCREEN_DATA) << 8; 
    ports::write_byte_to_port(REG_SCREEN_CTRL , 15 as u8);
    offset += ports::read_byte_from_port(REG_SCREEN_DATA);
    offset * 2;
}

pub fn clear_screen() {
    for _ in 0..BUFFER_HEIGHT {
        writeln!(WRITER.lock(), "");
    }
}

pub fn test_vga_write() {
    WRITER.lock().write_str("Hello! ");
    write!(WRITER.lock(), "The numbers are {} and {}", 42, 1.0/3.0);  
}



#[derive(Debug, Clone, Copy)]
struct ColorCode(u8);

impl ColorCode {
    const fn new(foreground: Color, background: Color) -> ColorCode {
        ColorCode((background as u8) << 4 | (foreground as u8))
    }
}

#[derive(Debug, Clone, Copy)]
#[repr(C)]
struct ScreenChar {
    ascii_character: u8,
    color_code: ColorCode,
}

struct Buffer {
    chars: [[Volatile<ScreenChar>; BUFFER_WIDTH]; BUFFER_HEIGHT],
}

pub struct Writer {
    column_position: usize,
    color_code: ColorCode,
    buffer: Unique<Buffer>,
}

impl Writer {
    pub fn write_byte(&mut self, byte: u8) {
        match byte {
            b'\n' => self.new_line(),
            byte => {
                if self.column_position >= BUFFER_WIDTH {
                    self.new_line();
                }

                let row = BUFFER_HEIGHT - 1;
                let col = self.column_position;

                let color_code = self.color_code;
                self.buffer().chars[row][col].write(ScreenChar {
                    ascii_character: byte,
                    color_code: color_code,
                });
                self.column_position += 1;
            }
        }
    }

    fn buffer(&mut self) -> &mut Buffer {
        unsafe{ self.buffer.get_mut() }
    }

    pub fn new_line(&mut self) {
        for row in 1..BUFFER_HEIGHT {
            for col in 0..BUFFER_WIDTH {
                let buffer = self.buffer();
                let character = buffer.chars[row][col].read();
                buffer.chars[row - 1][col].write(character);
            }
        }
        self.clear_row(BUFFER_HEIGHT-1);
        self.column_position = 0;
    }

    fn clear_row(&mut self, row: usize) {
        let blank = ScreenChar {
            ascii_character: b' ',
            color_code: self.color_code,
        };
        for col in 0..BUFFER_WIDTH {
            self.buffer().chars[row][col].write(blank);
        }
    }

    pub fn write_str(&mut self, s: &str) {
        for byte in s.bytes() {
          self.write_byte(byte)
        }
    }

    pub fn set_colors(&mut self, text_color: Color, background_color: Color) {
        self.color_code = ColorCode::new(text_color, background_color);
    }
}

impl fmt::Write for Writer {
    fn write_str(&mut self, s: &str) -> fmt::Result {
        for byte in s.bytes() {
          self.write_byte(byte)
        }
        Ok(())
    }
}