#![feature(conservative_impl_trait)]
#![feature(lang_items)]
#![feature(const_fn)]
#![feature(unique)]
#![feature(collections)]
#![feature(asm)]
#![no_std]

extern crate rlibc;
extern crate volatile;
extern crate spin;
extern crate multiboot2;
extern crate x86;
#[macro_use]
extern crate bitflags;

use core::fmt::Write;

mod solo_io;
use solo_io::vga;

mod solo_mem;
use solo_mem::frames;
use solo_mem::monitor::MemoryMonitor;
use solo_mem::api::MemoryConfig;
use solo_mem::frames::FrameAllocator;
use solo_mem::pages;

#[macro_use]
mod solo_util;

#[no_mangle]
pub extern fn rust_main(multiboot_information_address: usize) {
    vga::test_vga_write();    
    vga::clear_screen();

    let memory_config = MemoryConfig::new(multiboot_information_address);
    
    let mut memory_monitor = MemoryMonitor::new(&memory_config);
    memory_monitor.print_available_memory(&mut (*vga::WRITER.lock()));

    //let mut frame_allocator = frames::AreaFrameAllocator::new(&memory_config);
    //pages::test_paging(&mut frame_allocator);
}

#[lang = "eh_personality"] extern fn eh_personality() {}

#[lang = "panic_fmt"]
#[no_mangle]
pub extern fn panic_fmt(fmt: core::fmt::Arguments, file: &'static str,
    line: u32) -> !
{
    writeln!(vga::WRITER.lock(), "\n\nPANIC in {} at line {}:", file, line);
    writeln!(vga::WRITER.lock(), "    {}", fmt);
    loop{}
}

#[allow(non_snake_case)] #[no_mangle] pub extern "C" fn _Unwind_Resume() -> ! {loop {}}