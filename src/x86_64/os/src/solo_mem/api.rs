use multiboot2;
use multiboot2::MemoryAreaIter;
use multiboot2::ElfSectionIter;

pub struct MemoryConfig {
    pub multiboot_information_address: usize,
    pub kernel_start: u64,
    pub kernel_end: u64,
    pub multiboot_start: usize,
    pub multiboot_end: usize,
    memory_areas: MemoryAreaIter,
    kernel_elf_sections: ElfSectionIter,
}

impl MemoryConfig {
    pub fn new(multiboot_information_address: usize) -> MemoryConfig {
        let boot_info = unsafe{ 
            multiboot2::load(multiboot_information_address) 
        };

        let memory_map_tag = boot_info.memory_map_tag()
            .expect(",emory map tag required");

        let elf_sections_tag = boot_info.elf_sections_tag()
            .expect("elf-sections tag required");

        MemoryConfig {
            multiboot_information_address: multiboot_information_address,

            kernel_start: elf_sections_tag.sections().map(|s| s.addr)
                .min().unwrap(),
            kernel_end: elf_sections_tag.sections().map(|s| s.addr + s.size)
                .max().unwrap(),

            multiboot_start: multiboot_information_address,
            multiboot_end: multiboot_information_address + (boot_info.total_size as usize),

            memory_areas: memory_map_tag.memory_areas(),
            kernel_elf_sections: elf_sections_tag.sections(),
        }
    }
    
    pub fn memory_areas(&self) -> MemoryAreaIter { self.memory_areas.clone() }
    pub fn kernel_elf_sections(&self) -> ElfSectionIter { self.kernel_elf_sections.clone() }
}
