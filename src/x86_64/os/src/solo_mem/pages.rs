use solo_mem::frames::{PAGE_SIZE, Frame, FrameAllocator};
use solo_mem::entry::*;
use solo_mem::mapper::Mapper;

use solo_io::vga;

use core::ops::{Add, Deref, DerefMut};
use core::ptr::Unique;
use core::fmt::Write;


pub const ENTRY_COUNT: usize = 512;

pub type PhysicalAddress = usize;
pub type VirtualAddress = usize;


pub struct ActivePageTable {
    mapper: Mapper,
}

impl Deref for ActivePageTable {
    type Target = Mapper;

    fn deref(&self) -> &Mapper {
        &self.mapper
    }
}

impl DerefMut for ActivePageTable {
    fn deref_mut(&mut self) -> &mut Mapper {
        &mut self.mapper
    }
}

impl ActivePageTable {
    unsafe fn new() -> ActivePageTable {
        ActivePageTable { mapper: Mapper::new() }
    }
}


#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub struct Page {
   number: usize,
}

impl Page {
    pub fn containing_address(address: VirtualAddress) -> Page {
        assert!(address < 0x0000_8000_0000_0000 || address >= 0xffff_8000_0000_0000,
            "invalid address: 0x{:x}", address);
        Page { number: address / PAGE_SIZE }
    }

    pub fn start_address(&self) -> usize {
        self.number * PAGE_SIZE
    }

    pub fn p4_index(&self) -> usize {
        (self.number >> 27) & 0o777
    }
    pub fn p3_index(&self) -> usize {
        (self.number >> 18) & 0o777
    }
    pub fn p2_index(&self) -> usize {
        (self.number >> 9) & 0o777
    }
    pub fn p1_index(&self) -> usize {
        (self.number >> 0) & 0o777
    }

    pub fn range_inclusive(start: Page, end: Page) -> PageIter {
        PageIter {
            start: start,
            end: end,
        }
    }
}

impl Add<usize> for Page {
    type Output = Page;

    fn add(self, rhs: usize) -> Page {
        Page { number: self.number + rhs }
    }
}

#[derive(Clone)]
pub struct PageIter {
    start: Page,
    end: Page,
}

impl Iterator for PageIter {
    type Item = Page;

    fn next(&mut self) -> Option<Page> {
        if self.start <= self.end {
            let page = self.start;
            self.start.number += 1;
            Some(page)
        } else {
            None
        }
    }
}


pub fn test_paging<A>(allocator: &mut A) where A: FrameAllocator {
    let mut page_table = unsafe { ActivePageTable::new() };

    // test it
    // address 0 is mapped
    writeln!(vga::WRITER.lock(), "Some = {:?}", page_table.translate(0));
     // second P1 entry
    writeln!(vga::WRITER.lock(), "Some = {:?}", page_table.translate(4096));
    // second P2 entry
    writeln!(vga::WRITER.lock(), "Some = {:?}", page_table.translate(512 * 4096));
    // 300th P2 entry
    writeln!(vga::WRITER.lock(), "Some = {:?}", page_table.translate(300 * 512 * 4096));
    // second P3 entry
    writeln!(vga::WRITER.lock(), "None = {:?}", page_table.translate(512 * 512 * 4096));
    // last mapped byte
    writeln!(vga::WRITER.lock(), "Some = {:?}", page_table.translate(512 * 512 * 4096 - 1));


    let addr = 42 * 512 * 512 * 4096; // 42th P3 entry
    let page = Page::containing_address(addr);
    let frame = allocator.allocate_frame().expect("no more frames");
    writeln!(vga::WRITER.lock(), "None = {:?}, map to {:?}",
             page_table.translate(addr),
             frame);
    page_table.map_to(page, frame, EntryFlags::empty(), allocator);
    writeln!(vga::WRITER.lock(), "Some = {:?}", page_table.translate(addr));
    writeln!(vga::WRITER.lock(), "next free frame: {:?}", allocator.allocate_frame());

    page_table.unmap(Page::containing_address(addr), allocator);
    writeln!(vga::WRITER.lock(), "None = {:?}", page_table.translate(addr));
    writeln!(vga::WRITER.lock(), "{:#x}", unsafe {
        *(Page::containing_address(addr).start_address() as *const u64)
    });
}