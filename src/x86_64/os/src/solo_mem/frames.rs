use multiboot2::{MemoryArea};
use solo_mem::api::MemoryConfig;
use solo_mem::pages::PhysicalAddress;


pub const PAGE_SIZE: usize = 4096;


#[derive(Debug, PartialEq, Eq, PartialOrd, Ord)]
pub struct Frame {
    pub number: usize,
}

impl Frame {
    pub fn containing_address(address: usize) -> Frame {
        Frame{ number: address / PAGE_SIZE }
    }

    pub fn start_address(&self) -> PhysicalAddress {
        self.number * PAGE_SIZE
    }
}

pub trait FrameAllocator {
    fn allocate_frame(&mut self) -> Option<Frame>;
    fn deallocate_frame(&mut self, frame: Frame);
}

pub struct AreaFrameAllocator<'a> {
    next_free_frame: Frame,
    current_area: Option<&'a MemoryArea>,
    config: &'a MemoryConfig,
    kernel_start_frame: Frame,
    kernel_end_frame: Frame,
    multiboot_start_frame: Frame,
    multiboot_end_frame: Frame,
}

impl<'a> AreaFrameAllocator<'a> {
    pub fn new(config: &'a MemoryConfig) -> AreaFrameAllocator<'a>
    {
        let mut allocator = AreaFrameAllocator {
            next_free_frame: Frame::containing_address(0),
            current_area: None,
            config: config,
            kernel_start_frame: Frame::containing_address(config.kernel_start as usize),
            kernel_end_frame: Frame::containing_address(config.kernel_end as usize),
            multiboot_start_frame: Frame::containing_address(config.multiboot_start),
            multiboot_end_frame: Frame::containing_address(config.multiboot_end),
        };
        allocator.choose_next_area();
        allocator
    }

    fn config(&self) -> &'a MemoryConfig { self.config }

    fn choose_next_area(&mut self) {
        self.current_area = self.config().memory_areas().filter(|area| {
            let address = area.base_addr + area.length - 1;
            Frame::containing_address(address as usize) >= self.next_free_frame
        }).min_by_key(|area| area.base_addr);

        if let Some(area) = self.current_area {
            let start_frame = Frame::containing_address(area.base_addr as usize);
            if self.next_free_frame < start_frame {
                self.next_free_frame = start_frame;
            }
        }
    }
}

impl<'a> FrameAllocator for AreaFrameAllocator<'a> {
    fn allocate_frame(&mut self) -> Option<Frame> {
        if let Some(area) = self.current_area {
            // "Clone" the frame to return it if it's free. Frame doesn't
            // implement Clone, but we can construct an identical frame.
            let frame = Frame{ number: self.next_free_frame.number };

            // the last frame of the current area
            let current_area_last_frame = {
                let address = area.base_addr + area.length - 1;
                Frame::containing_address(address as usize)
            };

            if frame > current_area_last_frame {
                // all frames of current area are used, switch to next area
                self.choose_next_area();
            } else if frame >= self.kernel_start_frame && frame <= self.kernel_end_frame {
                // `frame` is used by the kernel
                self.next_free_frame = Frame {
                    number: self.kernel_end_frame.number + 1
                };
            } else if frame >= self.multiboot_start_frame && frame <= self.multiboot_end_frame {
                // `frame` is used by the multiboot information structure
                self.next_free_frame = Frame {
                    number: self.multiboot_end_frame.number + 1
                };
            } else {
                // frame is unused, increment `next_free_frame` and return it
                self.next_free_frame.number += 1;
                return Some(frame);
            }
            // `frame` was not valid, try it again with the updated `next_free_frame`
            self.allocate_frame()
        } else {
            None // no free frames left
        }
    }

    fn deallocate_frame(&mut self, frame: Frame) {
        unimplemented!()
    }
}