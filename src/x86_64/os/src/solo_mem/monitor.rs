use core::fmt::Write;

use solo_mem::api::MemoryConfig;


pub struct MemoryMonitor<'a> {
    config: &'a MemoryConfig,
}

impl<'a> MemoryMonitor<'a> {
    pub const fn new(config: &'a MemoryConfig) -> MemoryMonitor<'a> {
        MemoryMonitor { config: config }
    }

    fn config(&self) -> &'a MemoryConfig { self.config }

    pub fn print_available_memory<T: Write>(&mut self, writer: &mut T) {
        writeln!(*writer, "memory areas:");
        for area in self.config().memory_areas() {
            writeln!(writer, "    start: 0x{:x}, length: 0x{:x}",
                area.base_addr, area.length);
        }

        writeln!(writer, "kernel sections:");
        for section in self.config().kernel_elf_sections() {
            writeln!(writer, "    addr: 0x{:x}, size: 0x{:x}, flags: 0x{:x}",
                section.addr, section.size, section.flags);
        }

        writeln!(writer, "kernel start: {}, kernel end: {}", 
            self.config().kernel_start, self.config().kernel_end);
        writeln!(writer, "multiboot start: {}, multiboot end: {}", 
            self.config().multiboot_start, self.config().multiboot_end);
        writeln!(writer, "");
    }
}